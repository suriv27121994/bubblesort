package main

import (
	"fmt"
)

// child function
func bubbleSort(arr[] int) []int {
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr)-1; j++ {
			if arr[j] > arr[j+1] {
				arr[j], arr[j+1] = arr[j+1],
				arr[j]
			}			
		}
	}
	return arr
}
// MAIN function
func main() {
   arr:= []int{'g','u','s','t','i','a','r','s','y','a','d'};
   fmt.Println(bubbleSort(arr))
   }


